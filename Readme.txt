Il est préférable de laisser le dossier Common dans ce dossier, au même niveau que les autres dossiers.

Travail réalisé dans le cadre du cours : Intelligence Artificielle dans les jeux vidéos (8IAR125) à l'UQAC, pour le trimestre d'été 2021.
Membre de l'équipe :

- CHAMPION Melvin CHAM25069805
- LOISON Guillem LOIG14059903
- GUIVARCH Alan GUIA24119800
- GUYOT Sacha GUYS07119908
- NGUYEN Basile NGUB28099807
- LIN ​Stephane LINS02029809