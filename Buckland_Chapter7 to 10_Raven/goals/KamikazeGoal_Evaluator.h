#ifndef RAVEN_KAMIKAZE_EVALUATOR
#define RAVEN_KAMIKAZE_EVALUATOR
#pragma warning (disable:4786)
//-----------------------------------------------------------------------------
//
//  Name:   KamikazeGoal_Evaluator.h
//
//  Author: Basile Nguyen / Stephane Lin
//
//  Desc:  class to calculate how desirable the goal of auto explodes is
//-----------------------------------------------------------------------------

#include "Goal_Evaluator.h"
#include "../Raven_Bot.h"


class KamikazeGoal_Evaluator : public Goal_Evaluator
{ 
public:

  KamikazeGoal_Evaluator(double bias):Goal_Evaluator(bias){}
  
  double CalculateDesirability(Raven_Bot* pBot);

  void  SetGoal(Raven_Bot* pEnt);

  void RenderInfo(Vector2D Position, Raven_Bot* pBot);
};



#endif