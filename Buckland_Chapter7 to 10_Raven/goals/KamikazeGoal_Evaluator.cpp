#include "KamikazeGoal_Evaluator.h"
#include "Goal_Think.h"
#include "Raven_Goal_Types.h"
#include "misc/Stream_Utility_Functions.h"
#include "Raven_Feature.h"


#include "debug/DebugConsole.h"

//------------------ CalculateDesirability ------------------------------------
//
//  returns a value between 0 and 1 that indicates the Rating of a bot (the
//  higher the score, the stronger the bot).
//-----------------------------------------------------------------------------
double KamikazeGoal_Evaluator::CalculateDesirability(Raven_Bot* pBot)
{
    double health = Raven_Feature::Health(pBot);
    //maximum health limit allowed to activate auto destruction
    double maxHealthLimit = 0.3;

  if (health > maxHealthLimit)
  {   
      return 0;
  }
  else
  {
      double tweaker = 0.5;
      double Desirability = tweaker * (1 - Raven_Feature::Health(pBot));

      //ensure the value is in the range 0 to 1
      Clamp(Desirability, 0, 1);

      //bias the value according to the personality of the bot
      Desirability *= m_dCharacterBias;

      return Desirability;
  }
}

//----------------------------- SetGoal ---------------------------------------
//-----------------------------------------------------------------------------
void KamikazeGoal_Evaluator::SetGoal(Raven_Bot* pBot)
{
  pBot->GetBrain()->AddGoal_Kamikaze(); 
}

//-------------------------- RenderInfo ---------------------------------------
//-----------------------------------------------------------------------------
void KamikazeGoal_Evaluator::RenderInfo(Vector2D Position, Raven_Bot* pBot)
{
  gdi->TextAtPos(Position, "K: " + ttos(CalculateDesirability(pBot), 2));
  return;
    
  std::string s = ttos(Raven_Feature::Health(pBot));
  gdi->TextAtPos(Position+Vector2D(0,12), s);
}