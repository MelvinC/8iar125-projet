#include "Goal_Kamikaze.h"
#include "../Raven_Bot.h"
#include "../Raven_Game.h"
#include "../Raven_Messages.h"
#include "Messaging/Telegram.h"



//------------------------------- Activate ------------------------------------
//-----------------------------------------------------------------------------
void Goal_Kamikaze::Activate()
{
  m_iStatus = active;

  //if this goal is reactivated then there may be some existing subgoals that
  //must be removed
  RemoveAllSubgoals();

  Vector2D *zero = new Vector2D(0.001, 0.001);

  m_pOwner->GetWorld()->AddGrenade(m_pOwner, *zero);
  /*DECOMMENTEZ LORS DU MERGE LA GRENADE, N EXISTE PAS DANS LA BRANCHE BEHAVIOUR*/

  m_pOwner->SetDead();
}

//-------------------------- Process ------------------------------------------
//-----------------------------------------------------------------------------
int Goal_Kamikaze::Process()
{
  //if status is inactive, call Activate()
  ActivateIfInactive();
    

  return m_iStatus;
}


////---------------------------- HandleMessage ----------------------------------
////-----------------------------------------------------------------------------
//bool Goal_Kamikaze::HandleMessage(const Telegram& msg)
//{
//    //first, pass the message down the goal hierarchy
//    bool bHandled = ForwardMessageToFrontMostSubgoal(msg);
//
//    //if the msg was not handled, test to see if this goal can handle it
//    if (bHandled == false)
//    {
//        switch (msg.Msg)
//        {
//        case Msg_AttackThisBot:
//
//            //clear any existing goals
//            RemoveAllSubgoals();
//
//            m_pOwner->GetTargetSys()->SetTarget((Raven_Bot*)msg.ExtraInfo);
//            AddSubgoal(new Goal_AttackTarget(m_pOwner));
//
//            return true; //msg handle
//
//        default: return false;
//        }
//    }
//
//    //handled by subgoals
//    return true;
//}

