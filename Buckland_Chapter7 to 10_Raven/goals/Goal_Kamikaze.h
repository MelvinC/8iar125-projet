#ifndef GOAL_KAMIKAZE_H
#define GOAL_KAMIKAZE_H
#pragma warning (disable:4786)
//-----------------------------------------------------------------------------
//
//  Name:   Goal_Kamikaze.h
//
//  Author: Basile Nguyen / Stephane Lin
//
//  Desc:   
//
//-----------------------------------------------------------------------------
#include "Goals/Goal_Composite.h"
#include "Raven_Goal_Types.h"
#include "../Raven_Bot.h"


class Goal_Kamikaze : public Goal_Composite<Raven_Bot>
{
public:

  Goal_Kamikaze(Raven_Bot* pOwner):Goal_Composite<Raven_Bot>(pOwner, goal_kamikaze)
  {}

  void Activate();

  int  Process();


  //bool HandleMessage(const Telegram& msg);


  void Terminate(){m_iStatus = completed;}

};

#endif
